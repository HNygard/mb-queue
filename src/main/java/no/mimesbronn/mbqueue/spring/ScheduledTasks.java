package no.mimesbronn.mbqueue.spring;


import no.mimesbronn.mbqueue.service.IScraper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduledTasks {

    private static final Logger logger =
        LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat =
        new SimpleDateFormat("HH:mm:ss");

    private IScraper scraper;

    public ScheduledTasks(IScraper scraper) {
        this.scraper = scraper;
    }

    /**
     * Scrape mimesbrønn for content every evening at 23:00 .
     */
    @Scheduled(cron = "0 0 23 * * *")
    public void scrape() {
        logger.info("Starting scrape of mimesbrønn {}",
            dateFormat.format(new Date()));

        logger.info("Scraping not implemented yet. Placeholder code for later" +
            " implementation");

        logger.info("Finished scrape of mimesbrønn {}",
            dateFormat.format(new Date()));
    }
}
