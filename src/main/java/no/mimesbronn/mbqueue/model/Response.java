package no.mimesbronn.mbqueue.model;

// import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
public class Response
    extends RepresentationModel {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    /**
     * address of the response
     */
    @NotNull
    @Column(nullable = false)
    // @Audited
    private String url;

    /**
     * name of the correspondence party
     */
    @NotNull
    @Column
    // @Audited
    private String correspondenceParty;

    /**
     * mimesbronn id
     */
    @Column
    // @Audited
    private String mimesBronnId;

    /**
     * incoming/outgoing message id
     */
    @Column
    // @Audited
    private String messageId;

    /**
     * messageType (incoming/outgoing)
     */
    @Column
    // @Audited
    private String messageType;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime createdDate;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime mimesBronnCreatedDate;

    @NotNull
    @Column(nullable = false)
    // @Audited
    private boolean checked = false;

    // Link to request
    @ManyToOne
    @JoinColumn(name = "request_response_id", referencedColumnName = "id")
    private Request referenceRequest;

    // Link to comments
    @OneToMany(mappedBy = "referenceResponse")
    private List<Comment> referenceComments = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCorrespondenceParty() {
        return correspondenceParty;
    }

    public void setCorrespondenceParty(String correspondenceParty) {
        this.correspondenceParty = correspondenceParty;
    }

    public String getMimesBronnId() {
        return mimesBronnId;
    }

    public void setMimesBronnId(String mimesBronnId) {
        this.mimesBronnId = mimesBronnId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public OffsetDateTime getMimesBronnCreatedDate() {
        return mimesBronnCreatedDate;
    }

    public void setMimesBronnCreatedDate(OffsetDateTime mimesBronnCreatedDate) {
        this.mimesBronnCreatedDate = mimesBronnCreatedDate;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public List<Comment> getReferenceComments() {
        return referenceComments;
    }

    public void setReferenceComments(List<Comment> referenceComments) {
        this.referenceComments = referenceComments;
    }

    public void addReferenceComments(Comment comment) {
        this.referenceComments.add(comment);
    }

    public Request getReferenceRequest() {
        return referenceRequest;
    }

    public void setReferenceRequest(Request referenceRequest) {
        this.referenceRequest = referenceRequest;
    }

    @Override
    public String toString() {
        return "Response{" +
            "id=" + id +
            ", url='" + url + '\'' +
            ", correspondenceParty='" + correspondenceParty + '\'' +
            ", mimesBronnId='" + mimesBronnId + '\'' +
            ", messageId='" + messageId + '\'' +
            ", messageType='" + messageType + '\'' +
            ", createdDate=" + createdDate +
            ", mimesBronnCreatedDate=" + mimesBronnCreatedDate +
            ", checked=" + checked +
            '}';
    }
}
