package no.mimesbronn.mbqueue.model;

// import org.hibernate.envers.Audited;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Entity
public class MBUser
    extends RepresentationModel {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(unique = true)
    @NotNull
    // @Audited
    private String username;

    @Column(name = "password", length = 100)
    @NotNull
    @Size(min = 1, max = 100)
    private String password;

    @Column(name = "firstname", length = 50)
    @Size(min = 1, max = 50)
    private String firstname;

    @Column(name = "lastname", length = 50)
    @Size(min = 1, max = 50)
    private String lastname;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime createdDate;

    @Column
    private String createdBy;

    @Column
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime finalisedDate;

    @Column
    private String finalisedBy;

    @NotNull
    @Column(name = "account_non_expired", nullable = false)
    private boolean accountNonExpired = true;

    @NotNull
    @Column(name = "credentials_non_expired", nullable = false)
    private boolean credentialsNonExpired = true;

    @NotNull
    @Column(name = "account_non_locked", nullable = false)
    private boolean accountNonLocked = true;

    @Column(name = "enabled")
    @NotNull
    private Boolean enabled = true;

    @Column(name = "last_password_reset_date")
    @DateTimeFormat(iso = DATE_TIME)
    private OffsetDateTime lastPasswordResetDate;

    // Link to comments
    @OneToMany(mappedBy = "referenceMBUser")
    private List<Comment> referenceComments = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public OffsetDateTime getFinalisedDate() {
        return finalisedDate;
    }

    public void setFinalisedDate(OffsetDateTime finalisedDate) {
        this.finalisedDate = finalisedDate;
    }

    public String getFinalisedBy() {
        return finalisedBy;
    }

    public void setFinalisedBy(String finalisedBy) {
        this.finalisedBy = finalisedBy;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public OffsetDateTime getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(OffsetDateTime lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public List<Comment> getReferenceComments() {
        return referenceComments;
    }

    public void addReferenceComments(Comment comment) {
        this.referenceComments.add(comment);
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + id +
            ", username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", firstname='" + firstname + '\'' +
            ", lastname='" + lastname + '\'' +
            ", createdDate=" + createdDate +
            ", createdBy='" + createdBy + '\'' +
            ", finalisedDate=" + finalisedDate +
            ", finalisedBy='" + finalisedBy + '\'' +
            ", accountNonExpired=" + accountNonExpired +
            ", credentialsNonExpired=" + credentialsNonExpired +
            ", accountNonLocked=" + accountNonLocked +
            ", enabled=" + enabled +
            ", lastPasswordResetDate=" + lastPasswordResetDate +
            '}';
    }
}
