package no.mimesbronn.mbqueue.model;

// import org.hibernate.envers.Audited;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Comment
    extends RepresentationModel {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    /*
      The actual comment
     */
    @NotNull
    @Column(nullable = false)
    // @Audited
    private String title;

    // Link to response
    @ManyToOne
    @JoinColumn(name = "response_comment_id", referencedColumnName = "id")
    private Comment referenceResponse;

    // Link to request
    @ManyToOne
    @JoinColumn(name = "request_comment_id", referencedColumnName = "id")
    private Comment referenceRequest;

    // Link to user
    @ManyToOne
    @JoinColumn(name = "user_comment_id", referencedColumnName = "id")
    private MBUser referenceMBUser;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Comment getReferenceResponse() {
        return referenceResponse;
    }

    public void setReferenceResponse(Comment referenceResponse) {
        this.referenceResponse = referenceResponse;
    }

    public Comment getReferenceRequest() {
        return referenceRequest;
    }

    public void setReferenceRequest(Comment referenceRequest) {
        this.referenceRequest = referenceRequest;
    }

    public MBUser getReferenceMBUser() {
        return referenceMBUser;
    }

    public void setReferenceMBUser(MBUser referenceMBUser) {
        this.referenceMBUser = referenceMBUser;
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + id +
            ", title='" + title + '\'' +
            '}';
    }
}
