package no.mimesbronn.mbqueue.model;

// import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Note: Attribution to @HNygard
 */
@Entity
@JsonRootName("public_body")
public class PublicBody {

    @Id
    // @Audited
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(unique = true)
    @NotNull
    // @Audited
    private String name;

    @Column
    // TODO: This is not being picked up. Need to fix
    @JsonProperty("url_name")
    private String urlName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    @Override
    public String toString() {
        return "AlaveteliUser{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", urlName='" + urlName + '\'' +
            '}';
    }
}
