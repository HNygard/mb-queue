package no.mimesbronn.mbqueue.config;

public final class Constants {

    // REL values
    public static final String SELF_REL = "self";

    // Endpoint values
    public static final String USER = "user";
    public static final String RESPONSE = "response";
    public static final String SCRAPE = "scrape";

    // Endpoint pageable parameters
    public static final String PAGE = "page";
    public static final String SIZE = "size";
}
