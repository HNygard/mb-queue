package no.mimesbronn.mbqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MbQueueApplication {

	public static void main(String[] args) {
		SpringApplication.run(MbQueueApplication.class, args);
	}

}
