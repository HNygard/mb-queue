package no.mimesbronn.mbqueue.scraper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.SocketConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.h2.Driver;
import org.h2.jdbcx.JdbcDataSource;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scraper {
    private final CloseableHttpClient httpclient;
    private final Gson gson;


    public static void main(String[] args) throws IOException, SQLException {
        // :: Connect to database
        DriverManager.registerDriver(new Driver());
        JdbcDataSource jdbcDataSource = new JdbcDataSource();
        jdbcDataSource.setUrl("jdbc:h2:file:./mimesbronn.h2");
        jdbcDataSource.setUser("sa");
        jdbcDataSource.setPassword("");

        ScraperDb scraperDb = new ScraperDb(jdbcDataSource);
        scraperDb.createTablesIfNotExists();

        // :: Get some events
        Scraper scraper = new Scraper();

        scraper.getRequestFromHtml("https://www.mimesbronn.no/request/05042018_myre_fiskemottak_as_ins");

        scraper.scrapeNewMessagesFromMimesBronn(scraperDb, 21408, null);
    }

    public void scrapeNewMessagesFromMimesBronn(ScraperDb scraperDb, long eventIdStart, Integer optionalEventIdMax) throws IOException {
        long eventId = eventIdStart;
        Optional<Long> maxRequestEventId = scraperDb.getMaxRequestEventId();
        if (optionalEventIdMax == null && maxRequestEventId.isPresent()) {
            // -> We have requestEvents in database and are not running to a max limit
            eventId = maxRequestEventId.get().longValue();
        }


        eventId--;
        int requestEvents_on_thisRun = 0;
        while (true) {
            eventId++;
            requestEvents_on_thisRun++;

            if (optionalEventIdMax != null && eventId > optionalEventIdMax) {
                System.out.println("We are at the max event id [" + optionalEventIdMax + "]." +
                        " Halting scraper here to give the server a break.");
                break;
            }

            if (optionalEventIdMax == null && requestEvents_on_thisRun >= 250) {
                System.out.println("We have downloaded " + requestEvents_on_thisRun + " request events." +
                        " Halting scraper here to give the server a break.");
                break;
            }

            if (scraperDb.requestEventExists(eventId)) {
                // -> Request event already exists in database. No need to get it for Mimes Brønn again.
                continue;
            }
            Optional<RequestEvent> event = getEvent(eventId);
            if (event.isEmpty()) {
                System.out.println("[" + eventId + "] not present. Halting scraper here.");
                break;
            }

            RequestEvent requestEvent = event.get();
            if (requestEvent.messageType.isEmpty()) {
                scraperDb.insertRequstEvent_notMessage(eventId, "Found. Not message.");
                System.out.println("[" + eventId + "] not incoming/outgoing");
                continue;
            }

            if (
                    "https://www.mimesbronn.no/request/innsyn_lonn".equals(requestEvent.requestUrl)
            ) {
                System.out.println("[" + eventId + "] returns 500. Ignoring.");
                continue;
            }

            System.out.println("[" + eventId + "] is a message, getting full request");
            if (!scraperDb.requestExists(requestEvent.requestUrlTitle)) {
                // -> We don't have this request, let's get it.
                AlaveteliRequestDto requestDto = getRequestFromJson(requestEvent.requestUrl);
                AlaveteliRequestFromHtml requestFromHtml = getRequestFromHtml(requestEvent.requestUrl);

                if (!("https://www.mimesbronn.no/request/" + requestDto.url_title ).equals(requestFromHtml.requestUrl)) {
                    throw new RuntimeException("URL from JSON and URL from HTML mismatch.\n" +
                            "JSON .... : https://www.mimesbronn.no/request/" + requestDto.url_title + "\n" +
                            "HTML .... : " + requestFromHtml.requestUrl);
                }

                scraperDb.insertRequest(
                        requestDto.id,
                        requestDto.url_title,
                        requestDto.title,
                        requestDto.created_at,
                        requestDto.updated_at,
                        requestDto.user.url_name,
                        requestDto.public_body.url_name,
                        // Save a simple string of the dags
                        requestDto.tags.entrySet().stream()
                                .map(e -> e.getKey() + ":" + e.getValue())
                                .collect(Collectors.joining(" ")),
                        requestDto.json
                );

                for (AlaveteliCorrespondanceFromHtml correspondence : requestFromHtml.correspondence) {
                    scraperDb.insertRequestCorrespondence(
                            requestDto.id,
                            correspondence.incomingId,
                            correspondence.outgoingId,
                            correspondence.correspondenceHeader,
                            correspondence.correspondenceDateTime,
                            correspondence.correspondenceLink,
                            correspondence.correspondenceText);
                    for (AlaveteliAttachmentFromHtml attachment : correspondence.attachments) {
                        scraperDb.insertRequestCorrespondenceAttachment(
                                requestDto.id,
                                correspondence.incomingId != 0 ? correspondence.incomingId : correspondence.outgoingId,
                                attachment.fileNumber,
                                attachment.fileName,
                                attachment.fileMimeType,
                                attachment.fileSize,
                                attachment.linkDownload,
                                attachment.linkViewHtml
                        );
                    }
                }

                System.out.println("[" + eventId + "] Saved to database. [" + requestDto.updated_at + "] " + requestDto.title);

                // :: Log to find the event we are going to start checking from.
                long finalEventId = eventId;
                Stream.of(requestDto.info_request_events)
                        .filter(info_request_event -> info_request_event.id == finalEventId)
                        .forEach(info_request_event -> {
                            System.out.println("[" + finalEventId + "] ---" +
                                    " Event created at: [" + info_request_event.created_at + "]" +
                                    " event type [" + info_request_event.event_type + "].");
                        });

            }
            else {
                // TODO: We should update if we don't already have the event in the request
                // TODO: remember to set completed = false
            }



            scraperDb.insertRequestEvent(eventId,
                    "Found. Message.",
                    requestEvent.requestUrl,
                    requestEvent.requestUrlTitle,
                    requestEvent.messageType.get(),
                    requestEvent.messageId.get());
        }
    }

    public Scraper() {
        int CONNECTION_TIMEOUT_MS = 20000;
        SocketConfig socketConfig = SocketConfig.custom()
                .setSoTimeout(CONNECTION_TIMEOUT_MS)
                .build();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
                .setConnectTimeout(CONNECTION_TIMEOUT_MS)
                .setSocketTimeout(CONNECTION_TIMEOUT_MS)
                .build();

        httpclient = HttpClientBuilder.create()
                .disableRedirectHandling()
                .setDefaultSocketConfig(socketConfig)
                .setDefaultRequestConfig(requestConfig)
                .build();
        gson = new GsonBuilder()
                .create();
    }


    /**
     * Get a single Request Event from Alavateli. An event can be incoming or outgoing message, but also response
     * categorization (e.g. "this request was a success").
     *
     * @param eventId Event ID. As of 06.11.2019 Mimes Brønn have over 20000 events.
     * @return The event contain URL to parent, ID, etc
     */
    public Optional<RequestEvent> getEvent(long eventId) throws IOException {
        String url = "https://www.mimesbronn.no/request_event/" + eventId;
        HttpResponse response = httpclient.execute(auth(new HttpGet(url)));
        ((CloseableHttpResponse) response).close();

        if (response.getStatusLine().getStatusCode() == 404) {
            return Optional.empty();
        } else if (response.getStatusLine().getStatusCode() == 301) {
            return Optional.of(new RequestEvent(eventId, response.getFirstHeader("Location").getValue()));
        }

        throw new RuntimeException("Unknown response on [" + url + "]:\n"
                + Stream.of(response.getAllHeaders())
                .map(Object::toString)
                .collect(Collectors.joining("\n")));
    }

    /**
     * @param requestUrl URL for the request. E.g. https://www.mimesbronn.no/request/tilgangskoder_og_enheter_429#outgoing-5500
     */
    private AlaveteliRequestDto getRequestFromJson(String requestUrl) throws IOException {
        HttpResponse response = httpclient.execute(auth(new HttpGet(requestUrl + ".json")));

        String body = IOUtils.toString(response.getEntity().getContent(), "utf8");
        AlaveteliRequestDto requestDto = gson.fromJson(body, AlaveteliRequestDto.class);
        ((CloseableHttpResponse) response).close();
        if (requestDto == null) {
            System.err.println("Unable to get JSON from [" + requestUrl + ".json].");
        }
        requestDto.json = body;

        return requestDto;
    }

    private AlaveteliRequestFromHtml getRequestFromHtml(String requestUrl) throws IOException {
        HttpResponse response = httpclient.execute(auth(new HttpGet(requestUrl)));
        String body = IOUtils.toString(response.getEntity().getContent(), "utf8");
        ((CloseableHttpResponse) response).close();

        Document document = Jsoup.parse(body);


        AlaveteliRequestFromHtml requestFromHtml = new AlaveteliRequestFromHtml();

        // Get the canonical URL from meta tag:
        // <meta property="og:url" content="https://www.mimesbronn.no/request/<request URL>" />
        requestFromHtml.requestUrl = document.select("meta").stream()
                .filter(meta -> "og:url".equals(meta.attr("property")))
                .findFirst()
                .get()
                .attr("content");

        // <Name> sendte denne henvendelsen om innsyn etter <Law> til <Entity>
        Elements linksSubTitle = document.select(".request-header__subtitle a");
        requestFromHtml.userFullName = linksSubTitle.get(0).text();
        requestFromHtml.userUrl = linksSubTitle.get(0).attr("href").replaceAll("^/user/", "");
        requestFromHtml.publicBodyName = linksSubTitle.get(1).text();
        requestFromHtml.publicBodyUrl = linksSubTitle.get(1).attr("href").replaceAll("^/body/", "");

        // :: Get all correspondence back and forth
        // request --- 1:m --- correspondence --- 1:m --- files
        requestFromHtml.correspondence = new ArrayList<>();
        Elements allCorrespondence = document.select(".correspondence");
        for(Element correspondenceHtml : allCorrespondence) {
            AlaveteliCorrespondanceFromHtml correspondence = new AlaveteliCorrespondanceFromHtml();

            // :: Get type of correspondence + message id from id attribute
            String id = correspondenceHtml.attr("id");
            if (id.startsWith("outgoing-")) {
                correspondence.outgoingId = Integer.parseInt(id.substring("outgoing-".length()));
            }
            else if (id.startsWith("incoming-")) {
                correspondence.incomingId = Integer.parseInt(id.substring("incoming-".length()));
            }
            else {
                throw new RuntimeException("Unknown type of correspondence: " + id);
            }

            // :: Get header
            // This header contains different types of information that is a bit tricky to get out.
            Elements headerSelect = correspondenceHtml.select(".correspondence__header .correspondence__header__from--with-delivery-status");
            if(!headerSelect.isEmpty()) {
                // -> From header with delivery present
                correspondence.correspondenceHeader = headerSelect.first().textNodes().get(0).text().trim();
            }
            else {
                headerSelect = correspondenceHtml.select(".correspondence__header .correspondence__header__from");
                if(!headerSelect.isEmpty()) {
                    // -> From header with delivery present
                    correspondence.correspondenceHeader = headerSelect.first().textNodes().get(0).text().trim();
                }
                else {
                    // Unknown time. Use the whole thing.
                    correspondence.correspondenceHeader = correspondenceHtml.select(".correspondence__header").text();
                }
            }


            // <time datetime="2018-06-06T14:55:15+02:00" title="2018-06-06 14:55:15 +0200"> 6. juni 2018</time>
            correspondence.correspondenceDateTime = correspondenceHtml.select(".correspondence__header time")
                    .attr("datetime");


            // :: Content of the correspondence
            correspondence.correspondenceText = correspondenceHtml.select(".correspondence_text").text();

            // :: Direct link to this item
            // https://www.mimesbronn.no/request/<request URL>#incoming-1234
            // <input type="text" id="cplink__field" class="cplink__field" value="<THE URL>">
            correspondence.correspondenceLink = correspondenceHtml
                    .select(".correspondence__footer .cplink__field")
                    .first()
                    .attr("value");

            // :: Attachments
            //
            // Example
            //   <li class="attachment">
            //       <a href="/request/972/response/4638/attach/4/MYRE%20FISKEMOTTAK%20AS%20Tilsynsrapport%20ifbm%20gjennomf%20rt%20inspeksjon.PDF.pdf"><img alt="Attachment" class="attachment__image" src="/assets/icon_application_pdf_large-5ff7b47cebc4693cf729f854025d099f.png" /></a>
            //     <p class="attachment__name">MYRE FISKEMOTTAK AS Tilsynsrapport ifbm gjennomf rt inspeksjon.PDF.pdf</p>
            //     <p class="attachment__meta">
            //             64K
            //             <a href="/request/972/response/4638/attach/4/MYRE%20FISKEMOTTAK%20AS%20Tilsynsrapport%20ifbm%20gjennomf%20rt%20inspeksjon.PDF.pdf">Download</a>
            //         <a href="/request/972/response/4638/attach/html/4/MYRE%20FISKEMOTTAK%20AS%20Tilsynsrapport%20ifbm%20gjennomf%20rt%20inspeksjon.PDF.pdf.html">View as HTML</a>
            //       <!-- (application/pdf) -->
            //     </p>
            //   </li>
            correspondence.attachments = correspondenceHtml.select(".list-of-attachments .attachment").stream()
                .map(attHtml -> {
                    AlaveteliAttachmentFromHtml att = new AlaveteliAttachmentFromHtml();
                    att.fileName = attHtml.select(".attachment__name").text();

                    for (Element link : attHtml.select(".attachment__meta a")) {
                        if ("Download".equals(link.text())) {
                            att.linkDownload = "https://www.mimesbronn.no" + link.attr("href");
                        }
                        else if("View as HTML".equals(link.text())) {
                            att.linkViewHtml = "https://www.mimesbronn.no" + link.attr("href");
                        }
                        else {
                            throw new RuntimeException("Unknown attachment link: " + link.text());
                        }
                    }

                    // Find the attachment number in URL
                    Matcher matcher = Pattern
                            .compile("^https:\\/\\/www\\.mimesbronn\\.no\\/request\\/[0-9]*\\/response\\/[0-9]*\\/attach\\/([0-9]*)\\/")
                            .matcher(att.linkDownload);
                    if (!matcher.find()) {
                        throw new RuntimeException("Unable to find fileNumber in download link: " + att.linkDownload);
                    }
                    att.fileNumber = Integer.parseInt(matcher.group(1));

                    att.fileSize = attHtml.select(".attachment__meta").text()
                            .replace("Download", "")
                            .replace("View as HTML", "")
                            .trim();

                    // Find the comment node in DOM
                    Comment comment = (Comment) attHtml.select(".attachment__meta").first()
                            .childNodes().stream()
                            .filter(node -> node instanceof Comment).collect(Collectors.toList()).get(0);
                    // Example of comment:
                    // (text/html)
                    // (application/pdf)
                    att.fileMimeType = comment.getData().trim();
                    att.fileMimeType = att.fileMimeType.substring(1, att.fileMimeType.length() - 1);

                    return att;
                })
            .collect(Collectors.toList());

            requestFromHtml.correspondence.add(correspondence);
        }

        return requestFromHtml;
    }

    public static HttpRequestBase auth(HttpRequestBase httpRequest) {
        httpRequest.setHeader("Cookie", "mimesbronn=full_access");
        return httpRequest;
    }

    private static class AlaveteliRequestFromHtml {
        public String requestUrl;
        public String userFullName;
        public String userUrl;
        public String publicBodyName;
        public String publicBodyUrl;

        public List<AlaveteliCorrespondanceFromHtml> correspondence;
    }

    private static class AlaveteliCorrespondanceFromHtml {
        public int outgoingId;
        public int incomingId;
        public String correspondenceHeader;
        public String correspondenceDateTime;
        public String correspondenceText;
        public String correspondenceLink;
        public List<AlaveteliAttachmentFromHtml> attachments;
    }

    private static class AlaveteliAttachmentFromHtml {
        public int fileNumber;
        public String fileName;
        public String linkDownload;
        public String linkViewHtml;
        public String fileSize;
        public String fileMimeType;
    }

    private static class AlaveteliRequestDto {
        long id;
        String url_title;
        String title;
        String created_at;
        String updated_at;
        Map<String, Object> tags;
        AlaveteliUserDto user;
        AlaveteliPublicBodyDto public_body;
        AlaveteliRequestEventDto[] info_request_events;

        /**
         * Full JSON response from https://www.mimesbronn.no/request/<url_title>.json. The other fields are read from
         * this.
         */
        String json;
    }

    private static class AlaveteliUserDto {
        long id;
        String name;
        String url_name;
    }

    private static class AlaveteliRequestEventDto {
        long id;
        String event_type;
        long incoming_message_id;
        long outgoing_message_id;
        String created_at;
        String last_described_at;
    }

    private static class AlaveteliPublicBodyDto {
        long id;
        String name;
        String url_name;
    }

    public static class RequestEvent {
        private final long eventId;
        private final String requestUrl;
        private final String requestUrlTitle;
        private final Optional<String> messageType;
        private final Optional<String> messageId;

        public RequestEvent(long eventId, String urlWithHash) {
            this.eventId = eventId;

            // :: Regex examples
            // event id 20000 - event type sent, points to outgoing message id 5500
            //     https://www.mimesbronn.no/request/tilgangskoder_og_enheter_429#outgoing-5500
            // event id 21303 - event type status_update
            //     https://www.mimesbronn.no/request/orientering_om_skraping_av_einns
            Pattern pattern = Pattern.compile("^(https:\\/\\/www\\.mimesbronn\\.no\\/request\\/(([A-Za-z_0-9]*)))(#([A-Za-z_0-9]*)\\-([A-Za-z_0-9]*))?$");
            Matcher matcher = pattern.matcher(urlWithHash);
            if (!matcher.find()) {
                throw new RuntimeException("The URL [" + urlWithHash + "] did not match regex. Must likely adjust regex.");
            }
            this.requestUrl = matcher.group(1);
            this.requestUrlTitle = matcher.group(2);
            this.messageType = Optional.ofNullable(matcher.group(5));
            this.messageId = Optional.ofNullable(matcher.group(6));

            // Data quality, check that we know all types of messages so that our assumtion about event ids just being outgoing and incmming messages are true
            if (
                    this.messageType.isPresent()
                            && !this.messageType.get().equals("outgoing")
                            && !this.messageType.get().equals("incoming")
            ) {
                throw new RuntimeException("The URL [" + urlWithHash + "] did not return a known message type: " + this.messageType);
            }
        }
    }
}
