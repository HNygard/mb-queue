package no.mimesbronn.mbqueue.scraper;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class ScraperDb {

    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public ScraperDb(DataSource dataSource) {
        this(new JdbcTemplate(dataSource));
    }

    public ScraperDb(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public void createTablesIfNotExists() {
        try {
            jdbcTemplate.getDataSource().getConnection()
                    .prepareStatement("CREATE TABLE IF NOT EXISTS mb_request_events(" +
                            "eventId INT PRIMARY KEY," +
                            "status VARCHAR(20), " +
                            "requestUrl VARCHAR(255), " +
                            "requestUrlTitle VARCHAR(255), " +
                            "messageType VARCHAR(255), " +
                            "messageId VARCHAR(255)" +
                            ");").executeUpdate();
            jdbcTemplate.getDataSource().getConnection()
                    .prepareStatement("CREATE TABLE IF NOT EXISTS mb_requests(" +
                            "id INT PRIMARY KEY," +
                            "urlTitle VARCHAR(255) NOT NULL, " +
                            "title VARCHAR(255) NOT NULL, " +
                            "createdAt VARCHAR(255) NOT NULL, " +
                            "updatedAt VARCHAR(255) NOT NULL," +
                            "userUrlName VARCHAR(255) NOT NULL, " +
                            "publicBodyUrlName VARCHAR(255) NOT NULL, " +
                            "tags VARCHAR(255) NULL, " +
                            "json TEXT NOT NULL," +
                            "completed BIT NOT NULL" +
                            ");").executeUpdate();
            jdbcTemplate.getDataSource().getConnection()
                    .prepareStatement("CREATE TABLE IF NOT EXISTS mb_request_correspondence(" +
                            "requestId INT NOT NULL," +
                            "incomingId INT," +
                            "outgoingId INT," +
                            "correspondenceHeader VARCHAR(255) NOT NULL, " +
                            "correspondenceDateTime VARCHAR(255) NOT NULL, " +
                            "correspondenceLink VARCHAR(255) NOT NULL, " +
                            "correspondenceText TEXT NOT NULL" +
                            ");").executeUpdate();
            jdbcTemplate.getDataSource().getConnection()
                    .prepareStatement("CREATE TABLE IF NOT EXISTS mb_request_correspondence_attachments(" +
                            "requestId INT NOT NULL," +
                            "incomingOrOutgoingId INT NOT NULL," +
                            "fileNumber INT NOT NULL," +
                            "fileName VARCHAR(255) NOT NULL, " +
                            "fileMimeType VARCHAR(255) NOT NULL, " +
                            "fileSize VARCHAR(255) NOT NULL, " +
                            "linkDownload VARCHAR(255) NOT NULL, " +
                            "linkViewHtml VARCHAR(255)" +
                            ");").executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Unable to create tables.", e);
        }
    }

    public boolean requestEventExists(long eventId) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM mb_request_events WHERE eventId = ?",
                new Object[]{Long.valueOf(eventId)},
                Integer.class) > 0;
    }

    public void insertRequstEvent_notMessage(long eventId, String status) {
        Map<String, Object> params = new HashMap<>();
        params.put("eventId", eventId);
        params.put("status", status);
        namedJdbcTemplate.update("INSERT INTO mb_request_events (eventId, status) " +
                "VALUES (:eventId, :status)", params);
    }

    public void insertRequestEvent(long eventId, String status, String requestUrl, String requestUrlTitle,
                                   String messageType,
                                   String messageId) {
        Map<String, Object> params = new HashMap<>();
        params.put("eventId", eventId);
        params.put("status", status);
        params.put("requestUrl", requestUrl);
        params.put("requestUrlTitle", requestUrlTitle);
        params.put("messageType", messageType);
        params.put("messageId", messageId);
        namedJdbcTemplate.update("INSERT INTO mb_request_events" +
                " (eventId, status, requestUrl, requestUrlTitle, messageType, messageId) " +
                "VALUES (:eventId, :status, :requestUrl, :requestUrlTitle, :messageType, :messageId)", params);
    }

    public void insertRequest(long id, String urlTitle, String title, String createdAt, String updatedAt,
                              String userUrlName, String publicBodyUrlName, String tags, String json) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("urlTitle", urlTitle);
        params.put("title", title);
        params.put("createdAt", createdAt);
        params.put("updatedAt", updatedAt);
        params.put("userUrlName", userUrlName);
        params.put("publicBodyUrlName", publicBodyUrlName);
        params.put("tags", tags);
        params.put("json", json);
        params.put("completed", 0);
        namedJdbcTemplate.update("INSERT INTO mb_requests" +
                        " (id, urlTitle, title, createdAt, updatedAt, userUrlName, publicBodyUrlName, tags, json, completed) " +
                        "VALUES (:id, :urlTitle, :title, :createdAt, :updatedAt, :userUrlName, :publicBodyUrlName, :tags, :json, :completed)",
                params);
    }

    public void insertRequestCorrespondence(long requestId, int incomingId, int outgoingId, String correspondenceHeader,
                                            String correspondenceDateTime, String correspondenceLink,
                                            String correspondenceText) {
        Map<String, Object> params = new HashMap<>();
        params.put("requestId", requestId);
        params.put("incomingId", incomingId);
        params.put("outgoingId", outgoingId);
        params.put("correspondenceHeader", correspondenceHeader);
        params.put("correspondenceDateTime", correspondenceDateTime);
        params.put("correspondenceLink", correspondenceLink);
        params.put("correspondenceText", correspondenceText);
        namedJdbcTemplate.update("INSERT INTO mb_request_correspondence" +
                        " (requestId, incomingId, outgoingId, correspondenceHeader, correspondenceDateTime," +
                        " correspondenceLink, correspondenceText) " +
                        "VALUES (:requestId, :incomingId, :outgoingId, :correspondenceHeader, :correspondenceDateTime," +
                        " :correspondenceLink, :correspondenceText)",
                params);
    }

    public void insertRequestCorrespondenceAttachment(long requestId, int incomingOrOutgoingId, int fileNumber,
                                                      String fileName, String fileMimeType, String fileSize,
                                                      String linkDownload, String linkViewHtml) {
        Map<String, Object> params = new HashMap<>();
        params.put("requestId", requestId);
        params.put("incomingOrOutgoingId", incomingOrOutgoingId);
        params.put("fileNumber", fileNumber);
        params.put("fileName", fileName);
        params.put("fileMimeType", fileMimeType);
        params.put("fileSize", fileSize);
        params.put("linkDownload", linkDownload);
        params.put("linkViewHtml", linkViewHtml);
        namedJdbcTemplate.update("INSERT INTO mb_request_correspondence_attachments" +
                        " (requestId, incomingOrOutgoingId, fileNumber, fileName, fileMimeType," +
                        " fileSize, linkDownload, linkViewHtml) " +
                        "VALUES (:requestId, :incomingOrOutgoingId, :fileNumber, :fileName, :fileMimeType," +
                        " :fileSize, :linkDownload, :linkViewHtml)",
                params);
    }

    public boolean requestExists(String requestUrlTitle) {
        return jdbcTemplate.queryForObject("SELECT count(*) FROM mb_requests WHERE urlTitle = ?",
                new Object[]{requestUrlTitle},
                Integer.class) > 0;
    }

    public Optional<Long> getMaxRequestEventId() {
        return Optional.ofNullable(jdbcTemplate.queryForObject("SELECT max(eventId) FROM mb_request_events",
                Long.class));
    }

    public List<MbRequest> getRequests() {
        Map<String, Boolean> completed = new HashMap<>();
        jdbcTemplate.queryForList("SELECT * FROM request_reads" +
                " WHERE requestId IN (SELECT id FROM mb_requests WHERE completed = false)" +
                " ORDER BY requestReadTimestamp DESC").forEach(row -> {
            String key = null;
            if (row.get("syntheticAttachmentId") != null) {
                key = (String) row.get("syntheticAttachmentId");
            } else if (row.get("outgoingId") != null) {
                key = "outgoing-" + row.get("outgoingId");
            } else if (row.get("incomingId") != null) {
                key = "incoming-" + row.get("incomingId");
            }
            key = row.get("requestId") + "-" + key;
            if (!completed.containsKey(key)) {
                completed.put(key, (Boolean) row.get("completed"));
            }
        });

        List<MbRequest> mbRequests = jdbcTemplate.queryForList("SELECT * FROM mb_requests WHERE completed = false")
                .stream().map(row -> {
                    MbRequest request = new MbRequest();
                    request.requestId = ((Integer) row.get("id")).intValue();
                    request.requestTitle = (String) row.get("title");
                    request.url = "https://www.mimesbronn.no/request/" + row.get("urltitle");
                    request.createdAt = (String) row.get("createdAt");
                    request.updatedAt = (String) row.get("updatedAt");
                    request.userUrlName = (String) row.get("userUrlName");
                    request.publicBodyUrlName = (String) row.get("publicBodyUrlName");
                    request.tags = Arrays.stream(
                            ((String) row.get("tags")).split(" "))
                            .filter(tag -> !tag.trim().isEmpty())
                            // Tags can have a value. Remove the missing value (':null') on those that don't have value.
                            .map(tag -> tag.replaceFirst(":null$", ""))
                            // Create objects with extra info
                            .map(tag -> {
                                if (tag.startsWith("doc_num:")) {
                                    // TODO: this could link to Norske-postlister.no
                                    return new TagWithExtras(tag, "mb-tag-doc_num", "#/tag-doc_num");
                                } else {
                                    return new TagWithExtras(tag, "mb-tag-default", "#/tag-" + tag.split(":")[0]);
                                }
                            })
                            .collect(Collectors.toList());

                    if ("nor_p_batch".equals(request.userUrlName) && "Tilgangskoder og enheter".equals(request.requestTitle)) {
                        request.tags.add(new TagWithExtras("batch-tilgangskoder", "mb-tag-batch", "#/tag-batch-tilgangskoder"));
                    }
                    if ("tarjei_leer_salvesen".equals(request.userUrlName) && "Innsyn i reglement for folkevalgte".equals(request.requestTitle)) {
                        request.tags.add(new TagWithExtras("batch-reglement-folkevalgte", "mb-tag-batch", "#/tag-batch-reglement-folkevalgte"));
                    }

                    if (request.tags.isEmpty()) {
                        request.tags.add(new TagWithExtras("no-tags", "mb-tag-no-tag", "#/tag-no-tags"));
                    }

                    request.json = (String) row.get("json");
                    request.correspondence = new ArrayList<>();
                    return request;
                })
                .collect(Collectors.toList());
        List<Integer> requestIds = mbRequests.stream().map(r -> r.requestId).collect(Collectors.toList());
        Map<String, List<Integer>> parameters = new HashMap<>();
        parameters.put("requestIds", requestIds);

        // :: Get correspondence
        // ... and map into requests.
        jdbcTemplate.queryForList("SELECT corr.* FROM mb_request_correspondence corr" +
                " WHERE requestId IN (SELECT id FROM mb_requests WHERE completed = false)")
                .stream().map(row -> {
                    MbRequestCorrespondence corr = new MbRequestCorrespondence();
                    corr.requestId = ((Integer) row.get("requestId")).intValue();
                    corr.outgoingId = ((Integer) row.get("outgoingId")).intValue();
                    corr.incomingId = ((Integer) row.get("incomingId")).intValue();
                    corr.correspondenceHeader = (String) row.get("correspondenceHeader");
                    corr.correspondenceDateTime = (String) row.get("correspondenceDateTime");
                    corr.correspondenceLink = (String) row.get("correspondenceLink");

                    if (corr.outgoingId != 0) {
                        corr.completed  = completed.getOrDefault(corr.requestId + "-outgoing-" + corr.outgoingId, false);
                    }
                    if (corr.incomingId != 0) {
                        corr.completed  = completed.getOrDefault(corr.requestId + "-incoming-" + corr.incomingId, false);
                    }

                    corr.attachments = new ArrayList<>();
                    return corr;
                })
                // :: Map to the right request
                .forEach(corr -> mbRequests.stream()
                        .filter(r -> r.requestId == corr.requestId)
                        .forEach(request -> request.correspondence.add(corr))
                );

        // :: Get attachments
        // ... and map into correspondence on requests.
        jdbcTemplate.queryForList("SELECT att.* FROM mb_request_correspondence_attachments att" +
                " WHERE requestId IN (SELECT id FROM mb_requests WHERE completed = false)")
                .stream().map(row -> {
            MbRequestCorrespondenceAttachment att = new MbRequestCorrespondenceAttachment();
            att.requestId = ((Integer) row.get("requestId")).intValue();
            att.incomingOrOutgoingId = ((Integer) row.get("incomingOrOutgoingId")).intValue();
            att.fileNumber = ((Integer) row.get("fileNumber")).intValue();
            // TODO: move this to database. Primary key.
            att.attachmentId = att.requestId + "-" + att.incomingOrOutgoingId + "-" + att.fileNumber;
            att.fileName = (String) row.get("fileName");
            att.linkDownload = (String) row.get("linkDownload");
            att.linkViewHtml = (String) row.get("linkViewHtml");
            att.fileSize = (String) row.get("fileSize");
            att.fileMimeType = (String) row.get("fileMimeType");
            att.completed  = completed.getOrDefault(att.requestId + "-" + att.attachmentId, false);

            return att;
        })
                // :: Map to correspondence within the request
                // Yes, I know this looks terrible. Gets the job done.
                .forEach(att -> mbRequests.stream()
                        .filter(r -> r.requestId == att.requestId)
                        .forEach(request -> request.correspondence.stream()
                                .filter(corr -> corr.incomingId == att.incomingOrOutgoingId || corr.outgoingId == att.incomingOrOutgoingId)
                                .forEach(corr -> {
                                    corr.attachments.add(att);
                                })
                        )
                );

        return mbRequests;
    }

    public class MbRequest {
        public int requestId;
        public String requestTitle;
        public String url;
        public String createdAt;
        public String updatedAt;
        public String userUrlName;
        public String publicBodyUrlName;
        public List<TagWithExtras> tags;
        public String json;
        public List<MbRequestCorrespondence> correspondence;
    }

    public class TagWithExtras {
        public String tag;
        public String tagType;
        public String tagLink;

        public TagWithExtras(String tag, String tagType, String tagLink) {
            this.tag = tag;
            this.tagType = tagType;
            this.tagLink = tagLink;
        }
    }

    public class MbRequestCorrespondence {
        public int requestId;
        public int outgoingId;
        public int incomingId;
        public String correspondenceHeader;
        public String correspondenceDateTime;
        public String correspondenceLink;
        public boolean completed;
        public List<MbRequestCorrespondenceAttachment> attachments;
    }

    public class MbRequestCorrespondenceAttachment {
        public String attachmentId;
        public int requestId;
        public int incomingOrOutgoingId;
        public int fileNumber;
        public String fileName;
        public String linkDownload;
        public String linkViewHtml;
        public String fileSize;
        public String fileMimeType;
        public boolean completed;
    }
}
