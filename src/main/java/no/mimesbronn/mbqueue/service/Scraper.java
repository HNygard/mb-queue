package no.mimesbronn.mbqueue.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import no.mimesbronn.mbqueue.model.*;
import no.mimesbronn.mbqueue.repository.IPublicBodyRepository;
import no.mimesbronn.mbqueue.repository.IUserRepository;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static no.mimesbronn.mbqueue.scraper.Scraper.auth;

/**
 * Note: Attribution to @HNygard for providing code.
 **/
@Service
public class Scraper
    implements IScraper {

    private static final Logger logger =
        LoggerFactory.getLogger(Scraper.class);

    private final CloseableHttpClient httpclient;
    private final Gson gson;

    private IUserRepository userRepository;
    private IPublicBodyRepository publicBodyRepository;
    private IRequestService requestService;
    private IResponseService responseService;


    public Scraper(IUserRepository userRepository,
                   IRequestService requestService,
                   IResponseService responseService) {
        this.requestService = requestService;
        this.responseService = responseService;
        this.userRepository = userRepository;

        httpclient = HttpClientBuilder.create()
            .disableRedirectHandling()
            .build();
        gson = new GsonBuilder()
            .create();
    }

    @Override
    public void scrape(Integer eventId, Integer toId) throws IOException {
        logger.info("Scraper called. Starting from message request " +
            "(" + eventId + "), to (" + toId + ")");
        try {

            while (true) {
                Optional<AlaveteliEvent> event = getEvent(eventId++);


                if (eventId == 2643) {
                    logger.error("null request ... why??");
                }

                if (!event.isPresent()) {
                    logger.info("[" + eventId + "] not present");
                    break;
                }

                if (!event.get().getMessageType().isPresent()) {
                    logger.info("[" + eventId + "] not incoming/outgoing");
                }

                logger.info("[" + eventId + "] is a message, getting full request");
                Optional<Request> requestOptional =
                    getRequestFromEvent(event.get());

                if (eventId == 3505)
                    logger.info("Here");

                if (requestOptional.isPresent()) {
                    Request request = requestOptional.get();
                    persistUser(request);
                    persistPublicBody(request);

                    Response response = event.get().getResponse();
                    responseService.saveResponse(response);
                    request.addReferenceResponses(response);

                    requestService.saveRequest(request);
                }

                if (eventId >= toId) {
                    break;
                }
            }

        } catch (
            IOException e) {
            logger.error(e.getMessage());
        }

    }


    private void persistUser(Request request) {

        User user = request.getUser();
        if (user != null) {
            Optional userOpt =
                userRepository.findById(user.getId());
            if (userOpt.isEmpty()) {
                userRepository.save(user);
            }
        } else {
            logger.info("No user associated with " +
                request.getUrl());
        }
    }

    private void persistPublicBody(Request request) {
        PublicBody publicBody = request.getPublicBody();
        if (publicBody != null) {
            Optional publicBodyOpt =
                publicBodyRepository.findById(publicBody.getId());
            if (publicBodyOpt.isEmpty()) {
                publicBodyRepository.save(publicBody);
            }
        } else {
            logger.info("No publicBody associated with " +
                request.getUrl());
        }
    }

    /**
     * Check to see if the Request already exists in the database. If it does
     * retrieve and return. Otherwise create a new request and return.
     *
     * @param event
     * @return
     * @throws IOException
     */
    private Optional<Request> getRequestFromEvent
    (AlaveteliEvent event) throws IOException {

        logger.info("[" + event.getEventId() +
            "] is a message, getting full request");

        String url = event.getRequestUrl();
        final Pattern p = Pattern.compile("^(.*)#");
        Matcher m = p.matcher(url);
        if (m.find()) {
            url = m.group(1);
        }

        Optional<Request> request = requestService.getByUrl(url);

        if (request.isEmpty()) {
            Request r = getRequest(url);
            return request.ofNullable(r);
        }

        return request;
    }

    /**
     * @param requestUrl URL for the request.
     *                   E.g. https://www.mimesbronn.no/request/tilgangskoder_og_enheter_429#outgoing-5500
     */
    private Request getRequest(String requestUrl) throws IOException {
        org.apache.http.HttpResponse response =
            httpclient.execute(auth(new HttpGet(requestUrl + ".json")));

        String body = IOUtils.toString(response.getEntity().getContent(), "utf8");
        Request requestDto = gson.fromJson(body, Request.class);

        if (response.getStatusLine().getStatusCode() == 403) {
            requestDto = new Request();
            requestDto.setTitle("Hendvendelse som gir 403. Fjernet.");
        }
        requestDto.setUrl(requestUrl);

        if (requestDto == null) {
            logger.error(requestUrl + "gives null");
        }

        ((CloseableHttpResponse) response).close();
        return requestDto;
    }

    /**
     * Get a single Request Event from Alavateli. An event can be incoming or outgoing message, but also response
     * categorization (e.g. "this request was a success").
     */
    public Optional<AlaveteliEvent> getEvent(int eventId) throws
        IOException {
        String url = "https://www.mimesbronn.no/request_event/" + eventId;
        CloseableHttpResponse response =
            httpclient.execute(auth(new HttpGet(url)));
        response.close();

        if (eventId == 3535) {
            logger.error("stop");
        }

        if (response.getStatusLine().getStatusCode() == 404) {
            return Optional.empty();
        } else if (response.getStatusLine().getStatusCode() == 301) {
            return Optional.of(new AlaveteliEvent(
                eventId, response.getFirstHeader("Location").getValue()));
        }

        throw new RuntimeException("Unknown response on [" + url + "]:\n"
            + Stream.of(response.getAllHeaders())
            .map(Object::toString)
            .collect(Collectors.joining("\n")));
    }
}
