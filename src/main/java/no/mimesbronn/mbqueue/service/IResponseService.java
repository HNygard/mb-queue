package no.mimesbronn.mbqueue.service;

import no.mimesbronn.mbqueue.model.Response;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;

public interface IResponseService {

    Page<Response> getAllResponsesPaginated(Integer page, Integer size);

    Response getResponse(@NotNull Long responseId);

    Response saveResponse(@NotNull Response response);

    void deleteResponse(@NotNull Long responseId);

    Response updateResponse(@NotNull Long responseId,
                            @NotNull Response response);

    void verifyResponse(@NotNull Long responseId);
}
