package no.mimesbronn.mbqueue.repository;

import no.mimesbronn.mbqueue.model.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface IRequestRepository
    extends PagingAndSortingRepository<Request, Long> {
    Page<Request> findAll(Pageable pageable);
    Page<Request> findByTitleContaining(String query, Pageable pageable);

    Optional<Request> findByUrl(String url);
}
