package no.mimesbronn.mbqueue.repository;

import no.mimesbronn.mbqueue.model.PublicBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IPublicBodyRepository
    extends PagingAndSortingRepository<PublicBody, Long> {
    Page<PublicBody> findAll(Pageable pageable);

    Page<PublicBody> findByNameContaining(String query, Pageable pageable);
}
