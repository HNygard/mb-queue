package no.mimesbronn.mbqueue.repository;

import no.mimesbronn.mbqueue.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IUserRepository
    extends PagingAndSortingRepository<User, Long> {
    Page<User> findAll(Pageable pageable);

    Page<User> findByNameContaining(String query, Pageable pageable);
}
