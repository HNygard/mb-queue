package no.mimesbronn.mbqueue.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import no.mimesbronn.mbqueue.model.Request;
import no.mimesbronn.mbqueue.scraper.ScraperDb;
import no.mimesbronn.mbqueue.service.IRequestService;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class RequestController {

    private final IRequestService requestService;
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedJdbcTemplate;
    private final ScraperDb scraperDb;
    private final Gson gson;

    public RequestController(IRequestService requestService, JdbcTemplate jdbcTemplate) {
        this.requestService = requestService;
        this.jdbcTemplate = jdbcTemplate;
        this.namedJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.scraperDb = new ScraperDb(jdbcTemplate);

        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .registerTypeAdapter(LocalDateTime.class, (JsonSerializer<LocalDateTime>) (src, typeOfSrc, context) ->
                        new JsonPrimitive(src.format(DateTimeFormatter.ISO_DATE_TIME)))
                .create();
    }

    @PostConstruct
    public void init() throws SQLException {
        // Table containing all the registered 'reads' of attachments and messages.
        jdbcTemplate.getDataSource().getConnection()
                .prepareStatement("CREATE TABLE IF NOT EXISTS request_reads (" +
                        "requestId INT NOT NULL," +
                        "syntheticAttachmentId VARCHAR(255)," +
                        "outgoingId INT," +
                        "incomingId INT," +
                        "username VARCHAR(255) NOT NULL, " +
                        "comment TEXT, " +
                        "completed BIT NOT NULL, " +
                        "requestReadTimestamp TIMESTAMP NOT NULL" +
                        ");").executeUpdate();
    }

    @GetMapping("/request")
    public ResponseEntity<CollectionModel<Request>> getAllRequests(
        @RequestParam(required = false) String query,
        @RequestParam(defaultValue = "0", required = false) int page,
        @RequestParam(defaultValue = "10", required = false) int size) {
        var result = new CollectionModel<>(doQuery(query, page, size),
            linkTo(RequestController.class).withSelfRel());
        return ResponseEntity.status(OK).body(result);
    }

    @GetMapping(value = "/request/{requestId}")
    public ResponseEntity<Request> getRequest(
        @PathVariable("requestId") Long requestId) {
        Request request = requestService.getRequest(requestId);
        request.add(linkTo(RequestController.class).
            slash(request.getId()).withSelfRel());
        return ResponseEntity.status(OK).body(request);
    }

    @PutMapping(value = "/request/{requestId}")
    public ResponseEntity<Request> updateRequest(
        @PathVariable("requestId") Long requestId,
        @RequestBody Request updatedRequest) {
        Request request = requestService.updateRequest(requestId,
            updatedRequest);
        request.add(linkTo(RequestController.class).
            slash(request.getId()).withSelfRel());
        return ResponseEntity.status(OK)
            .body(request);
    }

    @PostMapping(value = "/request/{requestId}/verify")
    public ResponseEntity<Boolean> verifyRequest(
        @PathVariable("requestId") Long requestId) {
        requestService.verifyRequest(requestId);
        return ResponseEntity.status(OK).body(true);
    }

    @DeleteMapping(value = "/request/{requestId}")
    public ResponseEntity<Boolean> deleteRequest(
        @PathVariable("requestId") Long requestId) {
        requestService.deleteRequest(requestId);
        return ResponseEntity.status(OK).body(true);
    }

    @PostMapping(value = "/request-read")
    public ResponseEntity<Boolean> readRequest(@RequestBody RequestReadDto requestReadDto) {
        Map<String, Object> params = new HashMap<>();
        params.put("requestId", requestReadDto.requestId);
        params.put("username", requestReadDto.username);
        params.put("completed", requestReadDto.completed);
        params.put("requestReadTimestamp", Timestamp.valueOf(LocalDateTime.now()));

        if (requestReadDto.comment == null || requestReadDto.comment.trim().isEmpty()) {
            // -> Empty is null
            params.put("comment", null);
        }
        else {
            params.put("comment", requestReadDto.comment);
        }

        String idField;
        if (requestReadDto.attachmentId != null && !requestReadDto.attachmentId.isEmpty()) {
            idField = "syntheticAttachmentId";
            params.put("idFieldValue", requestReadDto.attachmentId);
        }
        else if(requestReadDto.outgoingId != 0) {
            idField = "outgoingId";
            params.put("idFieldValue", requestReadDto.outgoingId);
        }
        else if(requestReadDto.incomingId != 0) {
            idField = "incomingId";
            params.put("idFieldValue", requestReadDto.incomingId);
        }
        else {
            throw new RuntimeException("Request does not contain required ID.");
        }

        namedJdbcTemplate.update("INSERT INTO request_reads" +
                " (requestId, username, completed, comment, requestReadTimestamp, " + idField + ") " +
                "VALUES (:requestId, :username, :completed, :comment, :requestReadTimestamp, :idFieldValue)", params);

        // Get the data from database, including current status
        Map<String, Object> params2 = new HashMap<>();
        params2.put("requestId", requestReadDto.requestId);
        Optional<ScraperDb.MbRequest> mbRequest = scraperDb.getRequests().stream()
                .filter(r -> r.requestId == requestReadDto.requestId)
                .findFirst();
        if (mbRequest.isPresent()
                && !mbRequest.get().correspondence.stream().anyMatch(corr -> {
            if (!corr.completed) {
                return true;
            }

            return corr.attachments.stream().anyMatch(att -> !att.completed);
        })) {
            // -> All completed on this request => mark as completed
            namedJdbcTemplate.update("UPDATE mb_requests SET completed = true WHERE id = :requestId", params2);
        }
        else {
            namedJdbcTemplate.update("UPDATE mb_requests SET completed = false WHERE id = :requestId", params2);

        }


        return ResponseEntity.status(OK).body(true);
    }

    @GetMapping(value = "/request-reads")
    public ResponseEntity<String> getRequestReads(@RequestParam("30latest") String latest) {
        RequestReadDtos response = new RequestReadDtos();
        response.requestReads = jdbcTemplate.queryForList(
                "SELECT " + (latest != null ? "TOP 30 " : "")
                        + " rr.*," +
                        " mb_r.title as requestTitle," +
                        " mb_r.urltitle as requestUrlTitle" +
                        " FROM request_reads rr" +
                        " LEFT JOIN mb_requests mb_r ON mb_r.id = rr.requestId" +
                        " ORDER BY requestReadTimestamp DESC"
        )
                .stream().map(row -> {
                    RequestReadDto requestRead = new RequestReadDto();
                    requestRead.requestId = ((Integer) row.get("requestId")).longValue();
                    requestRead.attachmentId = (String) row.get("syntheticAttachmentId");
                    requestRead.outgoingId = row.get("outgoingId") != null ? ((Integer) row.get("outgoingId")).longValue() : 0;
                    requestRead.incomingId = row.get("incomingId") != null ? ((Integer) row.get("incomingId")).longValue() : 0;
                    requestRead.requestUrl = "https://www.mimesbronn.no/request/" + row.get("requestUrlTitle");
                    requestRead.requestTitle = (String) row.get("requestTitle");
                    requestRead.username = (String) row.get("username");
                    requestRead.comment = (String) row.get("comment");
                    requestRead.completed = "1".equals(row.get("completed"));
                    requestRead.requestReadTimestamp = ((Timestamp) row.get("requestReadTimestamp")).toLocalDateTime();
                    return requestRead;
                })
                .collect(Collectors.toList());
        // Fuck. Can't get the converters to work. AAAAAAH
        return ResponseEntity.status(OK).body(gson.toJson(response));
    }

    @GetMapping(value = "/mb-requests")
    public ResponseEntity<String> getMbRequests() {
        MbRequestsDto dto = new MbRequestsDto();
        dto.requests = scraperDb.getRequests();
        dto.tagGroups = dto.requests.stream()
                .map(r -> r.tags.stream()
                        .map(t -> t.tag.split(":")[0])
                        .collect(Collectors.toList())
                )
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

        // Fuck. Can't get the converters to work. AAAAAAH
        return ResponseEntity.status(OK).body(gson.toJson(dto));
    }

    private static class RequestReadDto {
        // Writable fields (must be public)
        public long requestId;
        public String attachmentId;
        public long outgoingId;
        public long incomingId;
        public String username;
        public boolean completed;
        public String comment;

        // Only on read, so keep them private
        private String requestUrl;
        private String requestTitle;
        private LocalDateTime requestReadTimestamp;
    }

    private static class RequestReadDtos {
        Collection<RequestReadDto> requestReads;
    }

    private static class MbRequestsDto {
        Collection<ScraperDb.MbRequest> requests;
        List<String> tagGroups;
    }

    private Page<Request> doQuery(String query, Integer page, Integer size) {
        if (query != null) {
            return requestService.
                getAllRequestsContainingPaginated(query, page, size);
        } else {
            return requestService.
                getAllRequestsPaginated(page, size);
        }
    }
}
