
// localStorage persistence
var STORAGE_KEY_NAME = 'mb-queue-name';
var nameStorage = {
    fetch: function () {
        return localStorage.getItem(STORAGE_KEY_NAME) || '';
    },
    save: function (name) {
        localStorage.setItem(STORAGE_KEY_NAME, name.trim())
    }
};

// visibility filters
var filters = {
    all: function (todos) {
        return todos
    },
    active: function (todos) {
        return todos.filter(function (todo) {
            return !todo.completed
        })
    },
    completed: function (todos) {
        return todos.filter(function (todo) {
            return todo.completed
        })
    },
    tag: function (requests, tagSearch) {
        if (tagSearch === '') {
            return requests;
        }

        return requests.filter(function (request) {
            if (request.tags.length === 0) {
                // -> Can't match. No tags.
                return false;
            }

            // Search for one or more matching tags. Match = starts with the 'search for tag'.
            return request.tags.filter(function (tag) {
                return tag.tag.substring(0, tagSearch.length) === tagSearch;
            }).length > 0;
        })
    }
};

var sendReadToBackend = function(request, postData) {
    axios
        .post('./request-read', postData)
        .then(function (response) {
            console.log('Logged OK');
            request.sent = true;
            request.sending = false;
            updateCompleted(request);
        })
        .catch(function (error) {
            request.sending = false;
            updateCompleted(request);
            console.log(error);
            alert(error);
        });
};

var updateCompleted = function(request) {
    // This reduce is not perfect. Not sure how to get it right...
    request.completed = request.correspondence.reduce(function (prev, corr) {
        if (!corr.completed) {
            return 1;
        }
        var attcount = corr.attachments.reduce(function (prev2, att) {
            if (!att.completed) {
                return 1;
            }
            return prev2;
        }, 0);
        return prev + attcount;
    }, 0) === 0;

    request.counterToTriggerRerenderOfComponent++;
    request.counterToTriggerRerenderOfComponent2 = request.counterToTriggerRerenderOfComponent + '-' + request.requestId;
};

// app Vue instance
var app = new Vue({
    renderError: function(h, err) {
        return h('pre', { style: { color: 'red' }}, err.stack)
    },

    // app initial state
    data: {
        alaveteliRequests: [],
        alaveteliRequestReads: [],
        alaveteliTagGroups: [],
        storedName: nameStorage.fetch(),
        visibility: 'all'
    },

    // watch todos change for localStorage persistence
    watch: {
        storedName: {
            handler: function (name) {
                nameStorage.save(name)
            }
        }
    },

    mounted: function() {
        var that = this;
        axios
            .get('./mb-requests')
            .then(function (response) {
                that.alaveteliRequests = response.data.requests.map(function (request) {

                    request.correspondence = request.correspondence.map(function (corr) {
                        // Simple date formatting without dragging in another library
                        // Start: 2019-10-03T23:00:05+02:00
                        corr.correspondenceDateTime = corr.correspondenceDateTime
                        // Remove timezone: 2019-10-03T23:00:05
                            .split('+')[0]
                        // Change the T into space: 2019-10-03 23:00:05
                            .replace('T', ' ');

                        // Adding a printout friendly link text
                        corr.correspondenceLinkText = corr.correspondenceLink.split('#')[1];
                        return corr;
                    });

                    // We increment this counter when we have updated the object.
                    request.counterToTriggerRerenderOfComponent = 0;
                    updateCompleted(request);

                    return request;
                });
                that.alaveteliTagGroups = response.data.tagGroups.map(function(tagGroup) {
                    return {
                        tagGroup: tagGroup,
                        tagGroupLink: '#/tag-' + tagGroup
                    };
                });
            })
            .catch(function (error) {
                console.log(error);
                alert(error);
            });
        axios
            .get('./request-reads?30latest=jepp')
            .then(function (response) {
                that.alaveteliRequestReads = response.data.requestReads.map(function (requestRead) {
                    // Simple date formatting without dragging in another library
                    // Start: 2019-11-15T20:35:58.788948
                    requestRead.requestReadTimestampFormatted = requestRead.requestReadTimestamp
                    // Remove millis: 2019-11-15T20:35:58
                        .split('.')[0]
                    // Change the T into space: 2019-11-15 20:35:58
                        .replace('T', ' ');

                    if (requestRead.attachmentId) {
                        requestRead.readInfo = 'attachment-' + requestRead.attachmentId;
                    }
                    else if (requestRead.outgoingId) {
                        requestRead.readInfo = 'outgoing-' + requestRead.outgoingId;
                    }
                    else if (requestRead.incomingId) {
                        requestRead.readInfo = 'incoming-' + requestRead.incomingId;
                    }
                    else {
                        requestRead.readInfo = '?'
                    }
                    return requestRead;
                });
            })
            .catch(function (error) {
                console.log(error);
                alert(error);
            });
    },

    // computed properties
    // http://vuejs.org/guide/computed.html
    computed: {
        filteredTodos: function () {
            if (this.visibility.substring(0, 4) === 'tag-') {
                return filters['tag'](this.alaveteliRequests, this.visibility.substring(4));
            }

            return filters[this.visibility](this.alaveteliRequests)
        },
        remaining: function () {
            return filters.active(this.alaveteliRequests).length
        }
    },

    filters: {
        pluralize: function (n) {
            return n === 1 ? 'item' : 'items'
        }
    },

    // methods that implement data logic.
    // note there's no DOM manipulation here at all.
    methods: {
        checkedRequestCorrespondance: function (request, corr) {
            var that = this;
            request.sending = true;
            request.sent = false;
            updateCompleted(request);
            sendReadToBackend(request, {
                requestId: request.requestId,
                outgoingId: corr.outgoingId,
                incomingId: corr.incomingId,
                username: that.storedName,
                completed: corr.completed
            });
        },
        checkedRequestAttachment: function (request, att) {
            var that = this;
            request.sending = true;;
            request.sent = false;
            updateCompleted(request);
            sendReadToBackend(request, {
                requestId: request.requestId,
                attachmentId: att.attachmentId,
                username: that.storedName,
                completed: att.completed
            });
        },

        removeCompleted: function () {
            this.alaveteliRequests = filters.active(this.alaveteliRequests)
        }
    },

    // a custom directive to wait for the DOM to be updated
    // before focusing on the input field.
    // http://vuejs.org/guide/custom-directive.html
    directives: {
        'todo-focus': function (el, binding) {
            if (binding.value) {
                el.focus()
            }
        }
    }
});

// handle routing
function onHashChange () {
    var visibility = window.location.hash.replace(/#\/?/, '');
    if (filters[visibility]) {
        app.visibility = visibility
    }
    else if(visibility.substring(0, 4) === 'tag-') {
        app.visibility = visibility
    }
    else {
        window.location.hash = '';
        app.visibility = 'all'
    }
}

window.addEventListener('hashchange', onHashChange);
onHashChange();

// mount
app.$mount('.mb-queue');
