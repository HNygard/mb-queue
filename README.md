# mb-queue

A spring-boot app to handle the management of messages for Mimes Brønn (Alaveteli).

URLs
- http://localhost:9889/mbqueue/ Main application
- http://localhost:9889/mbqueue/request-reads?30latest  Last 30 items marked as read
- http://localhost:9889/mbqueue/request-reads All items marked as read
- http://localhost:9889/mbqueue/scrape/scraper2?from=16000&to=16100 - Trigger scraping from event id 16000 to 16100

## Deploying

We use a simplified setup for building and deploying by using local machine. It is important there are now uncommitted files locally.

1. Make sure you don't any uncommited files
2. mvn install (or run mvn install from IntelliJ)
3. Test the JAR: java -jar target/mb-queue-0.0.1-SNAPSHOT.jar
4. curl http://localhost:9889/mbqueue/mb-requests
5. Run the following command to copy to production server and set as active:
export date=`date --iso-8601=minutes`; \
scp target/mb-queue-0.0.1-SNAPSHOT.jar "alaveteli-prod.nuug.no:/srv/mb-queue/jars/mb-queue-$date.jar"; \
ssh alaveteli-prod.nuug.no "rm /srv/mb-queue/mb-queue-current-version.jar; ln -s /srv/mb-queue/jars/mb-queue-$date.jar /srv/mb-queue/mb-queue-current-version.jar"
6. Start using:
cd /srv/mb-queue/; ./java/jdk-11.0.5+10/bin/java -jar mb-queue-current-version.jar